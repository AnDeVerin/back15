const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const { JWT_SECRET } = require('../config');
const BadRequestError = require('../errors/bad-request-error');
const NotFoundError = require('../errors/not-found-error');
const UnauthorizedError = require('../errors/unauthorized-error');

const login = (req, res, next) => {
  const { email, password } = req.body;
  return User.findUserByCredentials(email, password)
    .then((user) => {
      const token = jwt.sign({ _id: user._id }, JWT_SECRET);
      res
        .cookie('jwt', token, {
          maxAge: 3600000,
          httpOnly: true,
          sameSite: true,
        })
        .send({ data: user.toJSON() });
    })
    .catch((err) => {
      next(new UnauthorizedError(err.message));
    });
};

const createUser = (req, res, next) => {
  const { name = 'user', about = 'about', avatar, password, email } = req.body;
  return bcrypt.hash(password, 10).then((hash) =>
    User.create({
      name,
      about,
      avatar,
      email,
      password: hash,
    })
      .then((user) => res.status(201).send({ data: user.toJSON() }))
      .catch((err) => {
        if (err.name === 11000) {
          next(new ConflictError('ящик занят'));
        }
        if (err.name === 'ValidationError') {
          next(
            new BadRequestError(
              `${Object.values(err.errors)
                .map((error) => error.message)
                .join(', ')}`
            )
          );
        } else {
          next(err);
        }
      })
  );
};

const getUsers = (req, res, next) => {
  User.find({})
    .then((users) => res.send({ data: users }))
    .catch((err) => next(err));
};

const getUser = (req, res, next) => {
  const { id } = req.params;
  User.findById(id)
    .orFail(
      () => new NotFoundError('Пользователь по заданному id отсутствует в базе')
    )
    .then((users) => res.send({ data: users }))
    .catch((err) => {
      if (err.kind === 'ObjectId') {
        next(new BadRequestError('Невалидный id'));
      } else {
        next(err);
      }
    });
};

const updateUserData = (res, req, next) => {
  const {
    user: { _id },
    body,
  } = req;
  User.findByIdAndUpdate(_id, body, { new: true, runValidators: true })
    .orFail(
      () => new NotFoundError('Пользователь по заданному id отсутствует в базе')
    )
    .then((user) => res.send({ data: user }))
    .catch((err) => {
      if (err.name === 'ValidationError') {
        next(
          new BadRequestError(
            `${Object.values(err.errors)
              .map((error) => error.message)
              .join(', ')}`
          )
        );
      } else {
        next(err);
      }
    });
};

const updatedFields = ['name', 'about'];

// const commonUserUpdate = (req, res) => {
//   const { body } = req;
//   const emptyField = updatedFields.find((field) => body[field] === undefined);
//   if (emptyField && req.path === '/me') {
//     return res.status(400).send({ message: `Поле "${emptyField}" должно быть заполнено` });
//   }
//   if (!body.avatar && req.path === '/me/avatar') {
//     return res.status(400).send({ message: 'Поле "avatar" должно быть заполнено' });
//   }
//   return updateUserData(res, req);
// };

const updateUserInfo = (req, res, next) => {
  const { body } = req;
  const emptyField = updatedFields.find((field) => body[field] === undefined);
  if (emptyField) {
    return next(
      new BadRequestError(`Поле "${emptyField}" должно быть заполнено`)
    );
  }
  return updateUserData(res, req);
};

const updateUserAvatar = (req, res, next) => {
  const { body } = req;
  if (!body.avatar) {
    return next(new BadRequestError('Поле "avatar" должно быть заполнено'));
  }
  return updateUserData(res, req);
};

module.exports = {
  login,
  updateUserInfo,
  updateUserAvatar,
  createUser,
  getUsers,
  getUser,
};
