const router = require('express').Router();
const {
  getUser, updateUserInfo, updateUserAvatar, getUsers,
} = require('../controllers/users');
const { validateObjId } = require('../middlewares/validatons');

router.get('/', getUsers);
router.get('/:id', validateObjId, getUser);
router.patch('/me/avatar', updateUserAvatar);
router.patch('/me', updateUserInfo);

module.exports = router;
