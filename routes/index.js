const router = require('express').Router();
const userRouter = require('./users');
const cardRouter = require('./cards');
const auth = require('../middlewares/auth');
const checkPassword = require('../middlewares/check-password');

const {
  createUser, login,
} = require('../controllers/users');
const { validateUserBody, validateAuthentication } = require('../middlewares/validatons');

router.post('/signup', validateUserBody, checkPassword, createUser);
router.post('/signin', validateAuthentication, checkPassword, login);
router.use(auth);
router.use('/users', userRouter);
router.use('/cards', cardRouter);


module.exports = router;
