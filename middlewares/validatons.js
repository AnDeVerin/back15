const { Joi, celebrate } = require('celebrate');
const { ObjectId } = require('mongoose').Types;
const validator = require('validator');

const validateObjId = celebrate({
  params: Joi.object().keys({
    id: Joi.string()
      .required()
      .custom((value, helpers) => {
        if (ObjectId.isValid(value)) {
          return value;
        }
        return helpers.message('Невалидный id');
      }),
  }),
});

const validateCardBody = celebrate({
  body: Joi.object().keys({
    name: Joi.string().required().min(2).max(30).messages({
      'string.min': 'Минимальная длина поля "name" - 2',
      'string.max': 'Максимальная длина поля "name" - 30',
      'string.required': 'Поле "name" должно быть заполнено',
    }),
    link: Joi.string()
      .required()
      .custom((value, helpers) => {
        if (validator.isURL(value)) {
          return value;
        }
        return helpers.message('Поле "link" должно быть валидным url-адресом');
      })
      .messages({
        'string.required': 'Поле "link" должно быть заполнено',
      }),
  }),
});

const validateUserBody = celebrate({
  body: Joi.object().keys({
    // name: Joi.string().required().min(2).max(30)
    //   .messages({
    //     'string.min': 'Минимальная длина поля "name" - 2',
    //     'string.max': 'Максимальная длина поля "name" - 30',
    //     'any.required': 'Поле "name" должно быть заполнено',
    //   }),
    // about: Joi.string().required().min(2).max(30)
    //   .messages({
    //     'string.min': 'Минимальная длина поля "about" - 2',
    //     'string.max': 'Максимальная длина поля "about" - 30',
    //     'any.required': 'Поле "about" должно быть заполнено',
    //   }),
    password: Joi.string().required().messages({
      'any.required': 'Поле "password" должно быть заполнено',
    }),
    email: Joi.string()
      .required()
      .email()
      .message('Поле "email" должно быть валидным email-адресом')
      .messages({
        'any.required': 'Поле "email" должно быть заполнено',
      }),
    // avatar: Joi.string().required().custom((value, helpers) => {
    //   if (validator.isURL(value)) {
    //     return value;
    //   }
    //   return helpers.message('Поле "avatar" должно быть валидным url-адресом');
    // })
    //   .messages({
    //     'any.required': 'Поле "avatar" должно быть заполнено',
    //   }),
  }),
});

const validateAuthentication = celebrate({
  body: Joi.object().keys({
    email: Joi.string()
      .required()
      .email()
      .message('Поле "email" должно быть валидным email-адресом')
      .messages({
        'string.required': 'Поле "email" должно быть заполнено',
      }),
    password: Joi.string().required().messages({
      'any.required': 'Поле "password" должно быть заполнено',
    }),
  }),
});

module.exports = {
  validateObjId,
  validateCardBody,
  validateUserBody,
  validateAuthentication,
};
